const mongoose = require('mongoose');
const Schema = mongoose.Schema ;
const validator = require('validator');

const userSchema = new Schema({
            name:{
              type:String,
              required: true
            },
        
            email : {
            type : String,
            required : true,
            trim : true,
            unique : true,
            validate:{
                validator : validator.isEmail,
                message : '{value} is not a valid email address'
            }

        },
        password : {
            type: String,
            required : true,
            minlength : 6
        },
        tokens: [{
            access :{
                type : String,
                required : true

            },
            token:{
                type :String ,
                required : true

            }
        }]
}) ;


const User = mongoose.model('User', userSchema);
module.exports = User ;