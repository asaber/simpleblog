const mongoose = require('mongoose');
const Schema = mongoose.Schema ;

const commentsSchema = Schema({
    commenter: {
        type:String,
        required:true,
    },
    comtitle: {
        type:String,
    },
    combody: {
        type:String,
        required:true
    },
    comemail: {
        type:String,
        required: true
    },
    article :{
        type : Schema.Types.ObjectId,
        ref : 'Article'
    }
});


const Comments = mongoose.model('Comments', commentsSchema );

module.exports = Comments ;