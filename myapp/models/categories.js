const mongoose = require('mongoose');
const Schema = mongoose.Schema ;


var catSchema = Schema({
    name:{
        type : String,
        trim : true ,
        required : true,
        unique : true
    },
    slug:{
        type : String , 
        trim : true,
        required : true,
        unique : true
    },
    desc:{
        type : String,
        trim: true
    },
    post :{
        type : Schema.Types.ObjectId,
        ref : 'Article'
    }
});

const Cats = mongoose.model('Cats' , catSchema);

module.exports = Cats ;






