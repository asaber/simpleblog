const mongoose = require('mongoose');
const Schema = mongoose.Schema ;
//const Category = require('./categories');

var articleSchema = Schema({
    title : {
        type : String ,
        required : true 
    },
    slug : {
        type : String ,
        required : true 
    },
    postbody : {
        type : String ,
        required : true
    },
    auther :{
        type: String ,
        required : true
    },
    date:{
        type:Date,
        default: Date.now()
    },
    category : [{
        type:Schema.Types.ObjectId,
        ref : 'Cats'
    }],
    comments : [{
        type:Schema.Types.ObjectId,
        ref : 'Comments'
    }],
    tags : [{
        type:Schema.Types.ObjectId,
        ref : 'Tags'
    }],
    image :{
        type : String,
        default : 'imageurl'
    }

}) ;

var Article = mongoose.model('Article', articleSchema);

module.exports = Article;