const mongoose = require('mongoose');
const Schema = mongoose.Schema ;


const Tagschema = Schema({
    tag:{
        type : String,
        trim : true,
        unique:true,
        required : true
    },
    slug:{
        type : String,
        trim : true ,
        unique:true,
        required : true
    },
    desc:{
        type : String,
        trim : true
    },
    post :{
        type : Schema.Types.ObjectId,
        ref : 'Article'
    }
});

const Tag = mongoose.model('Tags', Tagschema);

module.exports = Tag ;