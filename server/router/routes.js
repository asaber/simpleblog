const express = require('express');
const router = express.Router();
const {ObjectID} = require('mongodb');
const Article = require('./../../myapp/models/article');
const Comment = require('./../../myapp/models/comments');
const User = require('./../../myapp/models/user');
const Tags = require('./../../myapp/models/tags');
const Cats = require('./../../myapp/models/categories');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const fs = require('fs');


console.log('hello from routes');


const multer = require('multer');
const path = require('path');


const storage = multer.diskStorage({
    destination : './public/uploads',
    filename : (req, file, cb)=>{
        cb(null, file.fieldname +'-' + Date.now() + path.extname(file.originalname));
    }
})

const upload = multer({ storage });


//  type   post 
//  desc   add new artice to Articles


router.post('/api/article', upload.single('selectedFile'), (req,res)=>{

    const title = req.body.title.trim() ;
    const slug = title.replace(/ /g,'-');
    const imgurl = req.file.filename ;

    //const imgurl = req.protocol + '://' + req.get('host') + '/uploads/' + req.file.filename ;

        //declare new article document
        var newarticle = new Article({
        title : req.body.title,
        slug : slug,
        postbody :req.body.postbody,
        auther : req.body.auther,
        image : imgurl 
        });

//console.log(req.file);

        if(req.body.tags !== undefined){
            // convert tags string into array
            req.body.tags.trim().split(",").map((tag)=>{
                    
                    //declear new tag document for each array element
                     var newtags = new Tags({
                    tag : tag.trim(),
                    slug : tag.trim().replace(/ /g,'-') 
                    })
                    newtags.save()
                    .then((tag)=>console.log(`tag saved : ${tag}`))
                    .catch((e)=>console.log(e))
                    newarticle.tags.push(newtags);

                    
                                                })


        }
        else if(req.body.tags.trim().length == 0){
        }

        if(req.body.category !== undefined){
            req.body.category.trim().split(",").map((cat)=>{
                var newcat = new Cats({
                    name : cat.trim(),
                    slug : cat.trim().replace(/ /g,'-')
                })
                newcat.save()
                .then((cat)=>{
                    console.log(`category saved ${ cat }`);
                })
                .catch((err)=>console.log(`error adding category is : err`));
                newarticle.category.push(newcat);
            })

        }

        newarticle.save()
        .then(()=> console.log('post saved succesfully'))
        .catch((err)=>console.log('error is : ', err))

      
      });



      
// type   get
// desc   show all articles ordered by new added and show 4 articles only

router.get('/api/article',(req,res)=>{
    Article.find().sort({_id:-1}).limit(4)
    .then((doc)=>{
    res.send(doc);
   },(err)=>{
console.log('error posting article', err );
res.status(400).send(err);
   });
});




//type   get
//desc   show article by id with associated comments


router.get('/api/article/:id',(req, res)=>{
// check if id is a valid object id
const id = req.params.id ;
 if(!ObjectID.isValid(id)){
   console.log('id is not valid');
     return res.status(404).json('id is not valid');
 }

Article.findOne({_id : req.params.id})
.populate('category')
.populate('tags')
.then((art)=>{
   res.json(art);
},(err)=>{
   console.log('error : ', err);
   
});
});



//type  get
//desc  show articles from specific auther

router.get('/api/aut/:id',(req,res)=>{
Article.find({auther : req.params.id})
.then((doc)=>{
   if(!doc){
       res.status(404).send('auther name is not valid')
   } else{
   res.json(doc);}
},(err)=>{
console.log('error posting article', err );
res.status(400).send(err);
});
})


// type  update
// desc  update specific post

router.patch('/api/article/:id', (req, res)=>{

} )

//type  delete
//desc  remove specific article 

router.delete('/api/article/:id',(req, res)=>{
const id = req.params.id ;

 if(!ObjectID.isValid(id)){
   console.log('id is not valid');
     return res.status(404).send('id is not valid');
 }
Article.findOneAndRemove({_id : id})
.then((deleted)=>{
   if(!deleted) { 
       console.log('id is not exist'); 
       return res.status(400).send('id is not exist'); }

   res.json(deleted);
   const img =path.join(  __dirname,'../..', '/public','/uploads/' ,deleted.image) ;
   fs.unlink(img , ()=>{console.log(`image deleted ${img}`)})


   

   //deleting associated comments
   if(deleted.comments){
   deleted.comments.map((com)=>{
       Comment.findOneAndRemove({_id : com._id})
       .then((comdel)=>console.log(`comment deleted ${comdel}`))
   },err=>console.log(err))
                       }

   //deleting associated image



},(err)=>{
   res.status(400).send(err);
});

})



// end of articles routes  -------------------------------------






// start comments routes   --------------------------------------




//  type   Post
//  desc   add new comment to post 

router.post('/api/article/addcomment/:id',(req, res)=>{
    //declare new comment document
    var newcomment = new Comment({
        commenter : req.body.commenter,
        combody : req.body.combody,
        comemail : req.body.comemail,
        comtitle : req.body.comtitle
    });

        newcomment.save().then((com)=>{
            console.log(com);

            //add comment to associated article
            Article.findOne({_id:req.params.id}).populate('Comments')
          .then((post)=>{post.comments.push(newcomment);
            return post.save()})
          .then((postcom)=>{
              res.json(postcom);
              console.log(postcom);
          })
        })
          
          .catch((err)=>{console.log(err)});
});


// type   get 
// desc   get all posts comments

router.get('/api/comments', (req, res)=>{
    console.log('hii from get comments');
    Comment.find()
    .then((t4)=>{
        res.send('great'); console.log(t4)},
        (err)=>console.log(`error loading comments: ${err}`))
});



// type   post
// desc   add new user


router.post('/api/register',(req, res)=>{

    User.findOne({email : req.body.email})
        .then((user)=>{
            if(user){
                console.log('email is already registered')
                return res.status(400).json('user already exist !')
            }

            var newuser = new User({
                name : req.body.name,
                email : req.body.email,
                password : req.body.password
            });
    
    
            bcrypt.genSalt(10, (err, salt)=>{
                bcrypt.hash(newuser.password, salt, (err, hash)=>{
                    if(err) {return err}
                    newuser.password = hash ;
    
                    newuser.save()
                    .then((user)=>{
                        console.log(user)
                        res.json(user)
                    })
                    .catch((err)=>{console.log(`error : ${ err }`)});
    
                })
            })

        })
 

        
}); 


// type  get 
// desc  user login using email and password

router.post('/api/login', (req, res)=>{
    var email = req.body.email
    var pass = req.body.password

    User.findOne({email})
    .then((user)=>{
        if(!user)return res.status(404).json('user not found!')
        bcrypt.compare(pass, user.password)
        .then((match)=>{
            if(!match) return res.status(404).json('email and password does not match!')

            const payload = { email : user.email, name : user.name, id:user._id}

            jwt.sign( payload, 'ram', {expiresIn : 3600}, (err, token)=>{
                res.json(`welcom you are logged in and token created : ${token}`)


            })


        })
    })
    
})

// type   post
// desc   add new tag

router.post('/api/addtag', (req, res)=>{
    var newtag = new Tags({
        tag : req.body.tag,
        slug : req.body.tag.trim().replace(/ /g,'-'),
        desc : req.body.desc
    });
    newtag.save()
    .then((tag)=>{console.log(tag);res.json('tag added')})
    .catch((err)=>console.log(err))
})



// type   post
// desc   add new category

router.post('/api/addcat', (req, res)=>{
    var newcat = new Cats({
        name : req.body.cat,
        slug : req.body.cat.trim().replace(/ /g,'-'),
        desc : req.body.desc
    });
    newcat.save()
    .then((cat)=>{console.log(cat);res.json('category added')})
    .catch((err)=>console.log(err))
})



// type   get
// desc   show all categories

router.get('/api/cats', (req, res)=>{
    Cats.find()
    .then((m)=>{
        res.json(m)
        console.log(m)
    })
    .catch(err => console.log(err))
})


// type   get
// desc   get specific category by id


router.get('/api/cat/:id', (req, res)=>{

    const id = req.params.id ;
      if(!ObjectID.isValid(id)){
        console.log('id is not valid');
        return res.status(404).send('id is not valid');
      }
      
    Cats.findById({_id : req.params.id})
    .then((cat)=>{console.log(cat);res.json(cat)})
    .catch((err)=>{console.log(err);res.status(400).json('err loading category')})
})


// type   patch
// desc   update categry 

router.patch('/api/cat/:id', (req,res)=>{
    
    const id = req.params.id ;
      if(!ObjectID.isValid(id)){
        console.log('id is not valid');
        return res.status(404).send('id is not valid');
      }
    Cats.findByIdAndUpdate(id, {$set:{name : req.body.name}},{new:true} )
    .then((cat)=>{console.log(cat);res.send('category edited')},
    (err)=>{console.log(`error is : ${err}`)})
});

//type   delete
//desc   remove specific category

router.delete('/api/cat/:id', (req, res)=>{
    const id = req.params.id ;
      if(!ObjectID.isValid(id)){
        console.log('id is not valid');
          return res.status(404).send('id is not valid');
      }

      Cats.findByIdAndRemove({_id:id})
      .then((cat)=>{console.log(`category deleted is ${cat}`);res.status(404).send('category deleted')}
      ,(err)=>{console.log(`error deleting ctegory : ${err}`)})
});





module.exports = {router};