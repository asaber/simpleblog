import React, { Component } from 'react';
import './App.css';

import Posts from './component/posts';
import PostPage from './component/postpage';
import Postbyauther from './component/postsbyauther';

import Err from './component/404'

import Addpost from './component/admin/addpost';
import Addtag from './component/admin/addtag' ;
import Addcat from './component/admin/addcat' ;
import Cats from './component/admin/editcats' ;

import Register from './component/admin/register';
import Login from './component/admin/login';
import Upload from './component/upload';
import Adminposts from './component/admin/viewposts'

import Cat from './component/admin/updatcat'





import Editor from  './component/quill';

import Navbar from './component/layout/Navbar';
import Footer from './component/layout/Footer';


import { BrowserRouter as Router, Route , Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
          <div>
        <Navbar/>
           <div className="container">
           <h1>my blog</h1>

           <Switch>
           <Route exact path='/' component= { Posts } />
           <Route path='/post/:id' component={ PostPage } />
           <Route exact path='/auther/:auth' component={ Postbyauther } />
           <Route exact path='/quill' component={ Editor } />
           
           <Route exact path='/addpost' component={ Addpost } />
           <Route exact path='/register' component={ Register } />
           <Route exact path='/login' component={ Login } />
           <Route exact path='/upload' component={ Upload } />
           <Route exact path='/addtag' component={ Addtag } />
           <Route exact path='/addcat' component={ Addcat } />
           <Route exact path='/editcat' component={ Cats } />
           <Route exact path='/cat/:id' component={ Cat } />
           <Route exact path='/adminposts' component={Adminposts} />

           <Route exact component={ Err }/>
           </Switch>
           </div>


        <Footer/>
          </div>
      </Router>
    );
  }
}

export default App;