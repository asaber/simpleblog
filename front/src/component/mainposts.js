import React, { Component } from 'react';
import {Link} from 'react-router-dom' ;


class mainposts extends Component{

  sn = this.props.obj.postbody.slice(0,120) ;

    render(){
        const imgurl = `http://localhost:5000/uploads/${this.props.obj.image}`

        return(
            <div className="col-lg-6  ">
            <div className="main-posts">

                <Link to={`/post/${this.props.obj._id}`} >
                <h2>{this.props.obj.title}</h2>
                <img src={imgurl} alt=""/>
                </Link>

                <Link to={`/auther/${this.props.obj.auther}`} >
                <span>{this.props.obj.auther}</span>
                </Link>
                <p>{`${this.sn} ... read more`}</p>
            </div>
            </div>

        );
    }
}

export default mainposts ;