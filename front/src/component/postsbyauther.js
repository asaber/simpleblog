import React,{Component} from 'react';
import axios from 'axios';
import Mainpostsbyauth from './mainpostsbyauth';


export default class postsbyauth extends Component {

    constructor(props){
        super(props);
        this.state = { posts : [] }
    }



componentDidMount() { 
    const ap = 'http://localhost:3000/api/';
    const param = this.props.match.params.auth;
    const url = `${ ap }${param}`;

    axios.get(url)
    .then( (res) => {
        this.setState({ posts : res.data } );
    })
    .catch(function (error) {
        console.log(error);
      });
    
}

postsformain(){
    return this.state.posts.map((object, i)=>{
        return <Mainpostsbyauth obj={object} key={i} />;
    });
}

render(){
    return (<div>
              <ul className="list-group">
                 {this.postsformain()}
              </ul>
            </div>
        );
}

}