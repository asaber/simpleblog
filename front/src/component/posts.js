import React, { Component } from 'react' ;
import axios from 'axios';
import Mainposts from './mainposts';

export default class ramram extends Component {

    constructor(props) {
        super(props);
        this.state = { posts : []};
        }

    componentDidMount() { 
        axios.get('/api/article')
        .then( (res) => {
            this.setState({ details : res.data } );

        })
        .catch(function (error) {
            console.log(error);
          })
        
    }

    postsformain(){
        return this.state.posts.map((object, i)=>{
            return <Mainposts obj={object} key={i} />;
        });
    }

    render(){
        return (
                 <div className="row">
                  {this.postsformain()}
                  </div>
                
            );
    }
}