import React from 'react';

export default ()=>{

    return (
        <footer className=" container-fluid page-footer font-small cyan darken-3 container-fluid">
        
            <div className="container">
        
              <div className="row">
        
                <div className="col-md-12 py-5">
                  <div className="mb-5 flex-center">
        
                    <a className="fb-ic">
                      <i className="fa fa-facebook fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <a className="tw-ic">
                      <i className="fa fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x "> </i>
                    </a>
                    <a className="gplus-ic">
                      <i className="fa fa-google-plus fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <a className="li-ic">
                      <i className="fa fa-linkedin fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <a className="ins-ic">
                      <i className="fa fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <a className="pin-ic">
                      <i className="fa fa-pinterest fa-lg white-text fa-2x"> </i>
                    </a>
                  </div>
                </div>
        
              </div>
        
            </div>
        
            <div className="footer-copyright text-center py-3">© 2018 Copyright:
              <a href=""> myblog</a>
            </div>
        
          </footer>
        
        
    );
}