import React,{Component} from 'react';
import Comment from './comments';
import Addcomment from './addcomment';
import axios from 'axios';


export default class postpage extends Component {

    constructor(props){
        super(props);
        this.state = { post : {},
                       newcomment : ''
                       }

        this.checkcomments = this.checkcomments.bind(this) ;
        this.newcommentadded = this.newcommentadded.bind(this);
        
    }

componentDidMount() { 
    const ap = '/api/article/';
    const param = this.props.match.params.id;
    const url = `${ ap }${param}`;

    axios.get(url)
    .then( (res) => {
        this.setState({ post : res.data } );

    })
    .catch(function (error) {
        console.log(error);
      })
    
}



checkcomments(){
    if(!this.state.post.comments){return 'no comments' ;}
    else{
        return this.state.post.comments.map((com, i)=>{
            return <Comment obj={com} key={i} />
        }) ;
    }
}

newcommentadded(){
    this.setState({newcomment : 'added'})
}

render(){
    const imgurl = `http://localhost:5000/uploads/${this.state.post.image}`

    return(
           <div>
               
                <h1> {this.state.post.title} </h1>
                <img alt="" width="150" height="150" src={imgurl} />
                <p>{this.state.post.postbody}</p>

                <hr></hr>
                <h5>Comments :</h5>
                <div>
                    <ul className="list-group">
                    {this.checkcomments()}
                    </ul>
                </div>

                <Addcomment obj={this.props.match.params.id} newcom={this.newcommentadded} />
           </div>
    );
}

}