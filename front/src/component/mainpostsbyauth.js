import React, { Component } from 'react';
import {Link} from 'react-router-dom' ;


class mainpostsbyauth extends Component{

    render(){
        return(
            
            <li className="list-group-item">

                <Link to={`../post/${this.props.obj._id}`} >
                <h2>{this.props.obj.title}</h2>
                </Link>

                
                <span>{this.props.obj.auther}</span>
                
            <p>{this.props.obj.postbody}</p>
            </li>
        );
    }
}

export default mainpostsbyauth ;