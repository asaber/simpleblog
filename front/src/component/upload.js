import React, { Component } from "react";
import axios from 'axios';


export default class Image extends Component {

    constructor() {
        super();
        this.state = {
          description: '',
          selectedFile: '',
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
      }


      onChange = (e) => {

        if(e.target.name === 'selectedFile')
        this.setState({ selectedFile: e.target.files[0] });
            
      }

      onSubmit(e){
        e.preventDefault();
        const { description, selectedFile } = this.state;
        let formData = new FormData();

        formData.append('description', description);
        formData.append('selectedFile', selectedFile);

        axios.post('upload', formData)
          .then((result) => {
            console.log(result);
          });

          this.setState({selectedFile : ''});
       
      }

render(){
    return (
    <div>
        <form  onSubmit={this.onSubmit}>
            <input type="file" name="selectedFile" onChange={this.onChange} />
            <input type="submit" value="Upload File" name="submit"/>
        </form>
    </div>
    )}
    

}


