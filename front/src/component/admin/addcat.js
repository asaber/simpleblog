import React, {Component} from 'react';
import axios from 'axios'

export default class Addcat extends Component {
    constructor(props){
        super(props);
        this.state = {
            cat : '',
            desc : ''
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    onChange(e){
        this.setState({
            [e.target.name] : e.target.value 
        })

    }

    onSubmit(e){
        e.preventDefault();
        axios.post('/api/addcat', {
            cat : this.state.cat,
            desc : this.state.desc
        })
        .then((cat)=>{console.log(cat);this.setState({
            cat : '',
            desc: ''
        })})
        .catch((err)=>console.log(err))

        

    }




    render(){
        return(

        <form onSubmit={this.onSubmit} >
        <label>
            Category Name:
            <input type="text" name="cat" value={this.state.cat} onChange={this.onChange}/>
        </label>
        <br></br>
        <label>
            Category Describtion:
            <input type="text" name="desc" value={this.state.desc} onChange={this.onChange}/>
        </label>
        <input type="submit" value="Submit" />
        </form>
        )}
}