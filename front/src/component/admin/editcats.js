import React, {Component} from 'react';
import axios from 'axios';
import Cat from './category'


export default class editclass extends Component{
    constructor(props){
        super(props);
        this.state = {cats : []}


    }


    componentDidMount(){

        axios.get('/api/ddd')
        .then((cat)=>{
            this.setState({
                cats : cat.data
            })
        },(err)=>{console.log(`error : ${err}`)})
    }

    indevcat(){
        return this.state.cats.map((cat,i)=>{
            return <Cat cat={cat} key={i} />;
        });
    }


    render(){
        return(
            <div className="container">
            <div className="list-group">
            {this.indevcat()}
            </div>
            </div>
        )

    }

}