import React from 'react'
import axios from 'axios'

export default class updatecat extends React.Component{

    constructor(props){
        super(props);
        this.state = {cat :{name:''}}

        this.handelupdate = this.handelupdate.bind(this);
        this.onChange = this.onChange.bind(this);

    }


componentDidMount(){
    axios.get(`/api/cat/${this.props.match.params.id}`)
    .then((cat)=>{this.setState({cat : cat.data})},
    (err)=>{console.log(`error getting category ${err}`)})
}    

handelupdate(e){
    e.preventDefault();
    axios.patch(`/api/cat/${this.props.match.params.id}`)
    .then(cat=>{console.log(cat)},
    (err)=>{console.log(`error getting category ${err}`)}

)
}

onChange(e){
    this.setState({
        cat: {[e.target.name] : e.target.value }
    })
}


    render(){
        return(
            <form onSubmit={this.handelupdate} >

                <div className="form-group">
                <label >Category Name</label>
                <input className="form-control form-control-lg" type="text" name="name"
                value={this.state.cat.name} onChange={this.onChange} />
                </div>

                <input className="btn btn-danger btn-lg" type="submit" value="Submit Edit"  />

            </form>
        );
    }
}