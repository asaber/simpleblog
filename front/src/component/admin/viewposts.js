import React , { Component } from 'react';
import Post from './post';
import axios from 'axios';


export default class extends Component {
    constructor(props){
        super(props);

        this.state = {
            posts : []
        }
    }

    componentDidMount(){
        axios.get('/api/article')
        .then((res)=>{
            this.setState({
                posts : res.data
            })
        })
    }

    postsforadmin(){
        return this.state.posts.map((pos, i)=>{
            return <Post obj={pos} key={i} />
        })
    }



    render(){
        return(
            <div className="container">
                <div className="row">
                  <div className="list-group">
                  {this.postsforadmin()}
                  </div>
                </div>
            </div>
        );
    }

}