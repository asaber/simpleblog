import React, {Component} from 'react';
//import axios from 'axios';


export default class register extends Component {

    constructor(props){
        super(props);

        this.state = {
            email:'',
            password:''
        }

        this.onChange = this.onChange.bind(this);
        this.handelSubmit = this.handelSubmit.bind(this);
    }
    

    onChange(e){
        this.setState({
            [e.target.name] : e.target.value
        });
    }

    handelSubmit(e){
        e.preventDefault();

    }


    render(){
        return (
            <div className='col-lg-6 align-middle' >
                <h2>Login</h2>

                <form onSubmit={this.handelSubmit} >

                    
                    <div className="form-group">
                    <label>Email</label>
                    <input className="form-control form-control-lg" type="email" name="email"
                    value={this.state.email} onChange={this.onChange} />
                    </div>
                    

                    <div className="form-group">
                    <label>Password</label>
                    <input className="form-control form-control-lg" type="password" name="password"
                    value={this.state.password} onChange={this.onChange} />
                    </div>


                    <input className="btn btn-primary btn-lg" type="submit" value="Sign In" />
                </form>
            </div>
        );
    }
}