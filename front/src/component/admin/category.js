import React, {Component} from 'react';
import axios from 'axios';


export default class Cat extends Component{
    constructor(props){
        super(props);

        this.removecat = this.removecat.bind(this);


    }



    removecat(){
        
        const url = 'api/cat/';
        const ur = url + this.props.cat._id ;
        axios.delete(ur)
        .then((del)=>{
            console.log(del);
        },(err)=>console.log(`error is : ${err}`))
        
    }


    render(){
   return (
       <div className="list-group-item">
          <p> {this.props.cat.name} </p>
           <input type="submit" value="Edit" />
           <input type="submit" className="btn btn-danger" value="Delete" onClick={this.removecat} />

       </div>
   )}

}

