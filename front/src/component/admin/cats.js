import React from 'react';
import axios from 'axios';
import Cat from './catscheckboxes';



export default class categories extends React.Component {

constructor(props){
    super(props);
    this.state = {cats:[]}
}

      //get all categories

      componentDidMount(){

        axios.get('api/cat')
        .then((cat)=>{
            this.setState({
                cats : cat.data
            });console.log('hiiii')
        },(err)=>{console.log(`error : ${err}`)})
    }

    indevcat(){
        return this.state.cats.map((cat,i)=>{
            return <Cat cat={cat} key={i} />;
        });
    }


    render(){
        return(
            <div>
                {this.indevcat()}
                
            </div>
        );
    }
}