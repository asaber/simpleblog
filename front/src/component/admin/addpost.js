import React, { Component } from 'react' ;
import axios from 'axios';
import Cat from './cats';

export default class addpost extends Component {
   
    constructor(props){
        super(props);
        this.state = { 
            title : '',
            postbody : '',
            selectedFile: '',
            auther : 'ramram',
            category : '',
            tags:''
     }
     this.onChange = this.onChange.bind(this);
     this.handelSubmit = this.handelSubmit.bind(this);
     this.title = '';
     this.postbody = '';
     this.saving = '';
     //this.cats = [1];

    }


  


    
    onChange(e){
        if(e.target.name === 'selectedFile')
        this.setState({ selectedFile: e.target.files[0] });

        else{
        this.setState({
            [e.target.name] : e.target.value,
        });
            }
    }

    handelSubmit(e){
        e.preventDefault();
        this.saving ='saving post ...'

        const { selectedFile, title, postbody, auther, category, tags } = this.state;
        let formData = new FormData();

        //adding state to form data

        formData.append('selectedFile', selectedFile);
        formData.append('title', title);
        formData.append('postbody', postbody);
        formData.append('auther', auther);
        formData.append('category', category);
        formData.append('tags', tags);




       axios.post('/api/article', formData
           // { title : this.state.title,
           // postbody :this.state.postbody,
           // auther : this.state.auther,
           // category : this.state.category,
           // tags : this.state.tags, }
                   )
        .then((res)=>{console.log(res.data);
            },
          (err)=>{console.log(err)})

          this.setState({
            title : '',
            postbody :'',
            auther : '',
            category : '',
            tags : ''
         });

       // this.props.history.push("/");
    }





    render(){
        console.log(this.cats);

        return(
        <div>
            <h2> Add New Post </h2>
            <form onSubmit={this.handelSubmit}>
                <div className="form-group">
                <label >title</label>
                <input className="form-control form-control-lg" type="text" name="title"
                placeholder="add post title here"
                value={this.state.title} onChange={this.onChange} />
                </div>
                
                <div className="form-group">
                <label >Add Post Body</label>
                <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" name="postbody"
                placeholder="add post contenet here" value={this.state.postbody} onChange={this.onChange} ></textarea> 
                </div>

                <div className="form-group">
                <label>Add image</label>
                <input type="file" name="selectedFile" accept="image/*"  onChange={this.onChange} />

                </div>

                <div className="form-group">
                <label >Tags</label>
                <input className="form-control form-control-lg" type="text" name="tags"
                placeholder="add tags here" value={this.state.tags} onChange={this.onChange} />
                </div>

                <div className="form-group">
                <label >category</label>
                <Cat />
                </div>




                <div className="form-group">
                <label >category</label>
                <input className="form-control form-control-lg" type="text" name="category"
                placeholder="add post title here"
                value={this.state.category} onChange={this.onChange} />
                </div>
                {this.saving}

                <input className="btn btn-primary btn-lg" type="submit" value="Submit" />
           </form>
        </div>
        );
    }
}