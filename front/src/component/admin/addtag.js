import React, {Component} from 'react';
import axios from 'axios'

export default class Addtag extends Component {
    constructor(props){
        super(props);
        this.state = {
            tag : '',
            desc : ''
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    onChange(e){
        this.setState({
            [e.target.name] : e.target.value 
        })

    }

    onSubmit(e){
        e.preventDefault();
        axios.post('/api/addtag', {
            tag : this.state.tag
        })
        .then((tag)=>{console.log(tag)})
        .catch((err)=>console.log(err))

        this.setState({
            tag : ''
        })

    }




    render(){
        return(

        <form onSubmit={this.onSubmit} >
        <label>
            Tag Name:
            <input type="text" name="tag" value={this.state.tag} onChange={this.onChange}/>
        </label>
        <label>
            Tag Describtion:
            <input type="text" name="desc" value={this.state.desc} onChange={this.onChange}/>
        </label>
        <input type="submit" value="Submit" />
        </form>
        )}
}