import React, {Component} from 'react';
import axios from 'axios';



export default class addcomment extends Component {

 constructor(props){
    super(props);
    this.state = {
                    commenter : '',
                    comtitle: '',
                    combody: '',
                    comemail:''
                       }

                this.onChange = this.onChange.bind(this);
                this.onSubmit = this.onSubmit.bind(this);

 }

 onChange(e){
    this.setState({
        [e.target.name] : e.target.value,
    });
}


 onSubmit(e){
    e.preventDefault();
    const url = '/api/article/addcomment/'
    const id = this.props.obj;

                 axios.post(`${url}${id}`, {commenter : this.state.commenter,
                    comtitle: this.state.comtitle,
                    combody: this.state.combody,
                    comemail: this.state.comemail} )
                    .then((res)=>{console.log(res);console.log(res.data);})
                    .catch((err)=>{console.log(err)});
    
    this.setState({
        commenter : '',
        comtitle: '',
        combody: '',
        comemail:''
    });

}

    render(){
        return(
            <div>

                <h5>add Comment :</h5>

                    <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label >Email address:</label>
                        <input type="email" className="form-control" name="comemail" value={this.state.commail} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label >Name:</label>
                        <input type="text" className="form-control" name="commenter" value={this.state.commenter} onChange={this.onChange} />
                    </div> 

                    <div className="form-group">
                        <label >comment title:</label>
                        <input type="text" className="form-control" name="comtitle" value={this.state.comtitle} onChange={this.onChange}/>
                    </div> 

                    <div className="form-group">
                        <label >Comment:</label>
                        <textarea className="form-control" rows="5"  name="combody" value={this.state.combody} onChange={this.onChange}></textarea>
                    </div>

                    <input type="submit" className="btn btn-default" value="submit" onClick={this.props.newcom} />

                    </form>
            </div>
        )
    }
}