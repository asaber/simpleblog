const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 5000 ;
const {mongoose} = require('./server/mongoose');
const {Article} = require('./myapp/models/article');
const {router} = require('./server/router/routes');
const path = require('path')




app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PATCH, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use(router);

app.use(express.static('./public'));

//app.use(express.static(path.join(__dirname, 'public')));



app.listen(port,()=>{
    console.log(`starting blog app on ${port}`);
    });